import React from 'react';
import useSWR from 'swr';

const fetcher = url => fetch(url).then(res => res.json());

const FruitList = () => {
    const { data: fruit, error, mutate } = useSWR('/api/list-fruit', fetcher, {
        refreshInterval: 2000, 
    });

    const handleDelete = async (id) => {
        const updatedFruits = fruit.filter(item => item.id !== id);
        mutate(updatedFruits, false);

        const response = await fetch(`/api/delete-fruit?id=${id}`, {
            method: 'DELETE',
        });

        if (!response.ok) {
            console.error('Failed to delete fruit');
            mutate();
            return;
        }
        mutate();
    };

    if (error) {
        return <p>Failed to fetch</p>;
    }

    if (!fruit) {
        return <p>Loading fruits...</p>;
    }

    return (
        <div className="table-container">
            <table>
                <thead>
                    <tr>
                        <th>Frucht</th>
                        <th>Lateinischer Name</th>
                        <th>Farbe</th>
                        <th>Herkunft</th>
                        <th>Kalorien pro 100g</th>
                        <th>Aktionen</th>
                    </tr>
                </thead>
                <tbody>
                    {fruit.length > 0 ? (
                        fruit.map((fruit) => (
                            <tr key={fruit.id}>
                                <td>{fruit["German Name"]}</td>
                                <td>{fruit["Latin Name"]}</td>
                                <td>{fruit.color}</td>
                                <td>{fruit.origin}</td>
                                <td>{fruit.calories}</td>
                                <td>
                                    <button onClick={() => handleDelete(fruit.id)}>Remove</button>
                                </td>
                            </tr>
                        ))
                    ) : (
                        <tr>
                            <td colSpan="6">No fruits available</td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    );
};

export default FruitList;
