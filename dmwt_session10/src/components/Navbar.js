import Link from 'next/link';
import { useRouter } from 'next/router';

const Navbar = () => {
  const router = useRouter();

  return (
    <>
      <nav className="navbar">
        <ul className="navList">
          <li className="navItem">
            <Link href="/" legacyBehavior>
              <a className={`navLink ${router.pathname === '/' ? 'active' : ''}`}>
                Main
              </a>
            </Link>
          </li>
          <li className="navItem">
            <Link href="/fruit" legacyBehavior>
              <a className={`navLink ${router.pathname === '/fruit' ? 'active' : ''}`}>
                Fruit
              </a>
            </Link>
          </li>
        </ul>
      </nav>
      <div style={{ height: '70px' }}></div>
    </>
  );
};

export default Navbar;
