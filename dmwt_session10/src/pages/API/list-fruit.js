import { sql } from '@vercel/postgres';

export default async function handler(request, response) {
    if (request.method !== 'GET') {
        return response.status(405).json({ error: 'Method not allowed' });
    }

    try {
        const { rows: fruit } = await sql`SELECT id, "German Name", "Latin Name", Color, Origin, Calories FROM essen;`;
        return response.status(200).json(fruit);
    } catch (error) {
        console.error('Error during request processing:', error);
        return response.status(500).json({ error: error.message });
    }
}
