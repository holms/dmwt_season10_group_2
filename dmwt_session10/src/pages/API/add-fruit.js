import { sql } from '@vercel/postgres';

export default async function handler(request, response) {
    if (request.method !== 'POST') {
        return response.status(405).json({ error: 'Method not allowed' });
    }

    try {
        const { germanName, latinName, color, origin, calories } = request.body;

        if (!germanName || !latinName || !color || !origin || !calories) {
            return response.status(400).json({ error: 'Alle Felder sind erforderlich.' });
        }

        const existingFruit = await sql`
            SELECT * FROM essen WHERE "German Name" = ${germanName};
        `;

        if (existingFruit.rows.length > 0) {
            
            return response.status(409).json({ error: 'Frucht schon vorhanden'});
        }

        const result = await sql`
            INSERT INTO essen ("German Name", "Latin Name", Color, Origin, Calories)
            VALUES (${germanName}, ${latinName}, ${color}, ${origin}, ${calories})
            RETURNING id, "German Name", "Latin Name", Color, Origin, Calories;
        `;

        const newFruit = result.rows[0];
        return response.status(200).json(newFruit);
    } catch (error) {
        console.error('Error during request processing:', error);
        return response.status(500).json({ error: error.message });
    }
}
