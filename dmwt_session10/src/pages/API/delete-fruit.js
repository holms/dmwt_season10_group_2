import { sql } from '@vercel/postgres';

export default async function handler(request, response) {
    if (request.method !== 'DELETE') {
        return response.status(405).json({ error: 'Method not allowed' });
    }

    const { id } = request.query;

    if (!id) {
        return response.status(400).json({ error: 'ID is required' });
    }

    try {
        const result = await sql`DELETE FROM essen WHERE id = ${id} RETURNING *;`;

        if (result.count === 0) {
            return response.status(404).json({ error: 'Fruit not found' });
        }

        return response.status(200).json({ message: 'Fruit deleted', fruit: result[0] });
    } catch (error) {
        console.error('Error during request processing:', error);
        return response.status(500).json({ error: error.message });
    }
}
