import { sql } from '@vercel/postgres';

export default async function handler(request, response) {
    try {
        const result = await sql`
            CREATE TABLE IF NOT EXISTS essen (

                id SERIAL PRIMARY KEY,
                "German Name" VARCHAR(255),
                "Latin Name" VARCHAR(255),
                 Color VARCHAR(255),
                 Origin VARCHAR(255),
                Calories INT
);`
        return response.status(200).json({ result });
    } catch (error) {
        console.error('Error creating table:', error);
        return response.status(500).json({ error: error.message });
    }
}
